
let student = [];


function addStudent(newStudent){
	student.push(newStudent);
}

addStudent("student-1");
addStudent("student-2");
addStudent("student-3");
addStudent("student-4");
console.log(student);

function printStudents(){
	
	student.forEach(
		function (element){
			console.log(element)
		}
	)
}

printStudents();

function countStudents(){
	console.log(student.length)
}

countStudents();

function findStudent(name){
	if (student.includes(name)){
		console.log( `${name} is in the student list.`)
	} else {
		console.log(`${name} is not found in the student list.`)
	}
}

findStudent("student-1");
findStudent("student-2");
findStudent("student-3");
findStudent("student-4");
findStudent("student-5");
findStudent("student-6");